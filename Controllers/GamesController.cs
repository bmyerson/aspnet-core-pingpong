using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using pong.Data;
using pong.Models;

namespace pong.Controllers
{
    [Route("api/[controller]")]
    public class GamesController : Controller
    {
        private PongContext _context;
        public GamesController(PongContext context)
        {
            _context = context;
        }
        // GET api/Games
        [HttpGet]
        public ActionResult Get()
        {
            return Json(_context.Games
                .Include(g => g.Player1)
                .Include(g => g.Player2)
                .Include(g => g.Table)
                .Select(g => new
                {
                    id = g.Id,
                    table = new
                    {
                        id = g.Table.Id,
                        name = g.Table.Name
                    },
                    player1 = new
                    {
                        id = g.Player1Id,
                        name = g.Player1.FirstName + " " + g.Player1.LastName,
                        score = g.Player1Points,
                        isWinner = g.Player1Points > g.Player2Points
                    },
                    player2 = new
                    {
                        id = g.Player2Id,
                        name = g.Player2.FirstName + " " + g.Player2.LastName,
                        score = g.Player2Points,
                        isWinner = g.Player2Points > g.Player1Points
                    },
                    time = String.Format("{0:G}", g.GameTime)
                }));
        }

        // GET api/Games/player/5
        [HttpGet("player/{id}")]
        public ActionResult GetGamesForPlayer(Guid id)
        {
            var games = _context.Games
                .Include(g => g.Player1)
                .Include(g => g.Player2)
                .Include(g => g.Table)
                .Where(g => g.Player1Id == id || g.Player2Id == id)
                .OrderBy(g => g.GameTime)
                .Select(game => new
                {
                    id = game.Id,
                    player1 = new
                    {
                        id = game.Player1Id,
                        name = game.Player1.FirstName + " " + game.Player1.LastName,
                        score = game.Player1Points,
                        isWinner = game.Player1Points > game.Player2Points
                    },
                    player2 = new
                    {
                        id = game.Player2Id,
                        name = game.Player2.FirstName + " " + game.Player2.LastName,
                        score = game.Player2Points,
                        isWinner = game.Player2Points > game.Player1Points
                    },
                    time = String.Format("{0:G}", game.GameTime),
                    table = new
                    {
                        id = game.Table.Id,
                        name = game.Table.Name,
                        description = game.Table.Description
                    }
                });
                int totalPoints = games.Sum(g => g.player1.id == id ? g.player1.score : g.player2.score);
                int winCount = games.Count(g => g.player1.id == id && g.player1.isWinner) + games.Count(g => g.player2.id == id && g.player2.isWinner);
                float winPercentage = (float)winCount / games.Count();
                float pointsPerGame = (float)totalPoints / games.Count();
            return Json(new
            {
                totalPoints = totalPoints,
                totalGames = games.Count(),
                totalWins = winCount,
                pointsPerGame = pointsPerGame,
                winPercentage = winPercentage * 100,
                games = games
            });
        }

        // GET api/Games/5
        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            Game game = _context.Games
                .Include(g => g.Player1)
                .Include(g => g.Player2)
                .Include(g => g.Table)

                .FirstOrDefault(g => g.Id == id);

            if (game == null)
            {
                return NotFound();
            }
            return Json(new
            {
                id = game.Id,
                player1 = new
                {
                    id = game.Player1Id,
                    name = game.Player1.FirstName + " " + game.Player1.LastName,
                    score = game.Player1Points,
                    isWinner = game.Player1Points > game.Player2Points
                },
                player2 = new
                {
                    id = game.Player2Id,
                    name = game.Player2.FirstName + " " + game.Player2.LastName,
                    score = game.Player2Points,
                    isWinner = game.Player2Points > game.Player1Points
                },
                time = String.Format("{0:G}", game.GameTime),
                table = new
                {
                    id = game.Table.Id,
                    name = game.Table.Name,
                    description = game.Table.Description
                }

            });
        }

        // POST api/Games
        [HttpPost]
        public void Post([FromBody]Game value)
        {
            _context.Games.Add(value);
            _context.SaveChanges();
        }

        // PUT api/Games/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody]Game value)
        {
            if (value.Id != null && value.Id != id)
            {
                BadRequest();
            }
            else
            {
                _context.Games.Update(value);
            }
            _context.SaveChanges();
        }

        // DELETE api/Games/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            Game game = _context.Games.FirstOrDefault(g => g.Id == id);
            if (game == null)
            {
                NotFound();
            }
            else
            {
                _context.Games.Remove(game);
            }
            _context.SaveChanges();
        }

    }
}
