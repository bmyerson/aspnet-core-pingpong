using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Mvc;
using pong.Data;
using pong.Models;

namespace pong.Controllers
{
    [Route("api/[controller]")]
    public class TablesController : Controller
    {
        private PongContext _context;
        public TablesController(PongContext context)
        {
            _context = context;
        }
        // GET api/tables
        [HttpGet]
        public ActionResult Get()
        {
            return Json(_context.Tables
                    .Include(t=>t.Games)
                    .Select(t => new {
                        id = t.Id,
                        name = t.Name,
                        description = t.Description,
                        numberOfGames = t.Games.Count()
                    }));
        }

        // GET api/tables/5
        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            Table table = _context.Tables
                    .Include(t=>t.Games)
                        .ThenInclude(g => g.Player1)
                    .Include(t=>t.Games)
                        .ThenInclude(g => g.Player2)
                    .FirstOrDefault(t => t.Id == id);
            if (table == null)
            {
                return NotFound();
            }
            return Json(new {
                id = table.Id,
                name = table.Name,
                description = table.Description,
                numberOfGames = table.Games.Count(),
                totalPointsScored = table.Games.Sum(g => g.TotalPoints()),
                latestGames = table.Games.OrderBy(g => g.GameTime).Reverse().Take(10).Select(g => new {
                    id = g.Id,
                    player1 = new {
                        id = g.Player1Id,
                        name = g.Player1.FirstName + " " + g.Player1.LastName,
                        score = g.Player1Points,
                        isWinner = g.Player1Points > g.Player2Points
                    },
                    player2 = new {
                        id = g.Player2Id,
                        name = g.Player2.FirstName + " " + g.Player2.LastName,
                        score = g.Player2Points,
                        isWinner = g.Player2Points > g.Player1Points
                    },
                    time = String.Format("{0:G}", g.GameTime) 
                })
            });
        }

        // POST api/tables
        [HttpPost]
        public void Post([FromBody]Table value)
        {
            _context.Tables.Add(value);
            _context.SaveChanges();
        }

        // PUT api/tables/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody]Table value)
        {
            if (value.Id != null && value.Id != id)
            {
                BadRequest();
            }
            else
            {
                _context.Tables.Update(value);
            }
            _context.SaveChanges();
        }

        // DELETE api/tables/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            Table table = _context.Tables.FirstOrDefault(t => t.Id == id);
            if (table == null)
            {
                NotFound();
            }
            else
            {
                _context.Tables.Remove(table);
            }
            _context.SaveChanges();
        }

    }
}
