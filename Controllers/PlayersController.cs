using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using pong.Data;
using pong.Models;

namespace pong.Controllers
{
    [Route("api/[controller]")]
    public class PlayersController : Controller
    {
        private PongContext _context;
        public PlayersController(PongContext context)
        {
            _context = context;
        }
        // GET api/players
        [HttpGet]
        public IEnumerable<Player> Get()
        {
            return _context.Players;
        }

        // GET api/players/5
        [HttpGet("{id}")]
        public ActionResult Get(Guid id)
        {
            Player player = _context.Players.FirstOrDefault(t => t.Id == id);
            if (player == null)
            {
                return NotFound();
            }
            return Json(player);
        }

        // POST api/players
        [HttpPost]
        public void Post([FromBody]Player value)
        {
            _context.Players.Add(value);
            _context.SaveChanges();
        }

        // PUT api/players/5
        [HttpPut("{id}")]
        public void Put(Guid id, [FromBody]Player value)
        {
            if (value.Id != null && value.Id != id)
            {
                BadRequest();
            }
            else
            {
                _context.Players.Update(value);
            }
            _context.SaveChanges();
        }

        // DELETE api/players/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            Player player = _context.Players.FirstOrDefault(t => t.Id == id);
            if (player == null)
            {
                NotFound();
            }
            else
            {
                _context.Players.Remove(player);
            }
            _context.SaveChanges();
        }

    }
}
