using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using pong.Models;


namespace pong.Data
{
    public class PongContext : DbContext
    {
        public PongContext(DbContextOptions<PongContext> options)
            : base(options)
        {
        }

        public DbSet<Player> Players { get; set; }
        public DbSet<Game> Games { get; set; }
        public DbSet<Table> Tables { get; set; }
    }
}