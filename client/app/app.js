    // dependencies
    var angular = require('angular'),
        angularUiRouter = require('angular-ui-router'),
        angularUiBootstrap = require('angular-ui-bootstrap'),

        // services
        tables = require('./services/table'),
        games = require('./services/game'),
        players = require('./services/player'),

        // components
        tableList = require('./components/table/list'),
        tableDetail = require('./components/table/detail'),
        tableCreate = require('./components/table/create')

    playerList = require('./components/player/list'),
        playerDetail = require('./components/player/detail')
    playerCreate = require('./components/player/create')

    gameList = require('./components/game/list'),
        gameDetail = require('./components/game/detail')
    gameCreate = require('./components/game/create');

    console.log(angularUiBootstrap);
    var app = angular.module('pongApplication', [
            'ui.router',
            'ui.bootstrap'
        ])
        .service('tableService', tables)
        .service('gameService', games)
        .service('playerService', players)

        .component('tableList', tableList)
        .component('tableDetail', tableDetail)
        .component('tableCreate', tableCreate)

        .component('playerList', playerList)
        .component('playerDetail', playerDetail)
        .component('playerCreate', playerCreate)

        .component('gameList', gameList)
        .component('gameDetail', gameDetail)
        .component('gameCreate', gameCreate);

    app.config(function ($stateProvider) {
        // An array of state definitions
        var states = [{
                name: 'tables',
                url: '',
                abstract: true,
                template: '<ui-view/>'
            },
            {
                name: 'tables.tables',
                url: '',
                component: 'tableList',
                resolve: {
                    tables: function (tableService) {
                        return tableService.getAllTables().then(function (res) {
                            return res;
                        });
                    }
                }
            },
            {
                name: 'tables.table',
                url: 'table/{tableId}',
                component: 'tableDetail',
                resolve: {
                    table: function (tableService, $transition$) {
                        return tableService.get($transition$.params().tableId).then(function (res) {
                            return res;
                        });
                    }
                }
            },

            {
                name: 'tables.tables.new',
                url: '/new',
                onEnter: function ($transition$, $uibModal, $state, tableService) {
                    $uibModal.open({
                        component: 'tableCreate'
                    }).result.then(function (res) {
                        // submit form 
                        return tableService.addTable(res);
                    }).catch(function (err) {
                        // error or dismissal
                        console.log('dismissed', err);
                    }).finally(function () {
                        $state.go('^', {}, {reload:true});
                    });
                }
            },

            {
                name: 'tables.table.new',
                url: '/new',
                onEnter: function ($transition$, $uibModal, $state, gameService, tableService, playerService) {
                    var tableId = $transition$.params().tableId;
                   
                    $uibModal.open({
                        component: 'gameCreate',
                        resolve: {
                            tableId: function() {
                                return tableId;
                            },
                            players: function()  {
                                return playerService.getAllPlayers().then(function (res) {
                                    return res;
                                });
                            }
                        }
                    }).result.then(function (res) {
                        // submit form 
                        return gameService.addGame(res);
                    }).catch(function (err) {
                        // error or dismissal
                        console.log('dismissed', err);
                    }).finally(function () {
                        $state.go('^', {}, {reload:true});
                    });
                }
            },
            {
                name: 'players',
                url: '/players',
                component: 'playerList',
                resolve: {
                    players: function (playerService) {
                        return playerService.getAllPlayers();
                    }
                }
            },
            {
                name: 'players.new',
                url: '/new',
                onEnter: function ($transition$, $uibModal, $state, playerService) {
                    var tableId = $transition$.params().tableId;
                   
                    $uibModal.open({
                        component: 'playerCreate'
                    }).result.then(function (res) {
                        // submit form 
                        return playerService.addPlayer(res);
                    }).catch(function (err) {
                        // error or dismissal
                        console.log('dismissed', err);
                    }).finally(function () {
                        $state.go('^', {}, {reload:true});
                    });
                }
            },
            {
                name: 'player',
                url: '/players/{playerId}',
                component: 'playerDetail',
                resolve: {
                    player: function (playerService, $transition$) {
                        return playerService.get($transition$.params().playerId);
                    },
                    games: function (gameService, $transition$) {
                        return gameService.getGamesForPlayer($transition$.params().playerId);
                    },
                }
            }

        ];

        // Loop over the state definitions and register them
        states.forEach(function (state) {
            $stateProvider.state(state);
        });

    });