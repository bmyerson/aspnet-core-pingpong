module.exports = {
    bindings: {
      modalInstance: '<',
      resolve: '<'
    },
    templateUrl: '/templates/game/create.html',
    controller: function ($state) {
        var vm = this;

        console.log(vm.resolve, this.resolve);

        vm.newGame = {
            player1Id: '',
            player2Id: '',
            player1Points: 0,
            player2Points: 0
        };

        vm.handleOk = function () {
            vm.newGame.tableId = vm.resolve.tableId;
            vm.modalInstance.close(vm.newGame);
        }

        vm.handleCancel = function () {
            vm.modalInstance.dismiss('Cancel');
        }

    }
}