module.exports = {
    bindings: {
        tables: '<'
    },
    templateUrl: '/templates/table/list.html',
    controller: function ($state) {
        var vm = this;

        this.goToTable = function (table) {
            $state.go('tables.table', {tableId: table.id});
        }
    }
}