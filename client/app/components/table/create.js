module.exports = {
    bindings: {
      modalInstance: '<'
    },
    templateUrl: '/templates/table/create.html',
    controller: function () {
        var vm = this;

        vm.newTable = {
            name: '',
            description: ''
        };

        vm.handleOk = function () {
            vm.modalInstance.close(vm.newTable);
        }

        vm.handleCancel = function () {
            vm.modalInstance.dismiss('Cancel');
        }

    }
}