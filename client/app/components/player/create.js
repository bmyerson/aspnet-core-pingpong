module.exports = {
    bindings: {
      modalInstance: '<'
    },
    templateUrl: '/templates/player/create.html',
    controller: function () {
        var vm = this;

        vm.newPlayer = {
            name: '',
            description: ''
        };

        vm.handleOk = function () {
            vm.modalInstance.close(vm.newPlayer);
        }

        vm.handleCancel = function () {
            vm.modalInstance.dismiss('Cancel');
        }

    }
}