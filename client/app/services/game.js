'use strict';
var angular = require('angular');

module.exports = ['$http', function gameService($http) {
    var service = {};

    service.getAllGames = function getAllGames() {
        return $http.get('/api/games').then(function (res) {
            return res.data;
        });
    };

    service.getGamesForPlayer = function getGamesForPlayer(playerId) {
        return $http.get('/api/games/player/' + playerId).then(function (res) {
            return res.data;
        })
    }

    service.addGame = function addGame(game) {
        return $http.post('/api/games', game).then(function (res) {
            return res.data;
        });

    };

    return service;
}];