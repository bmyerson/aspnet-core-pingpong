'use strict';
var angular = require('angular');

module.exports = ['$http', function playerService($http) {
    var service = {};

    service.getAllPlayers = function getAllPlayers() {
        return $http.get('/api/players').then(function (res) {
            return res.data;
        });
    };

    service.get = function get(id) {
        return $http.get('/api/players/' + id).then(function (res) {
            return res.data;
        });
    };

    service.addPlayer = function addPlayer(player) {
        return $http.post('/api/players', player).then(function(res) {
            return res;
        });
    };


    return service;
}];