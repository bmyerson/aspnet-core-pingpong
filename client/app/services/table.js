'use strict';
var angular = require('angular');

module.exports = ['$http', function tableService($http) {
    var service = {};

    service.getAllTables = function getAllTables() {
        return $http.get('/api/tables').then(function (res) {
            return res.data;
        });
    };

    service.get = function get(tableId) {
        return $http.get('/api/tables/' + tableId).then(function (res) {
            return res.data;
        });
    }

    service.addTable = function addTable(table) {
        console.log('adding table', table);
        return $http.post('/api/tables', table).then(function(res) {
            return res;
        });
    };

    service.updateTable = function updateTable(table) {
        return $http.put('/api/tables/' + table.id, table);
    }

    return service;
}];