using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using pong.Data;
using pong.Models;

namespace pong
{
    public class Startup
    {
        public static Random rand = new Random();
            
        public Startup(IHostingEnvironment env)
        {
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();
            Configuration = builder.Build();
        }

        public IConfigurationRoot Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<PongContext>(opt => opt.UseInMemoryDatabase());

            // Add framework services.
            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));
            loggerFactory.AddDebug();

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseBrowserLink();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }

            app.UseStaticFiles();

            var context = app.ApplicationServices.GetService<PongContext>();
            AddTestData(context);

            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
        private static void AddTestData(PongContext context)
        {
            Player[] players = {
                new Player
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Daenerys",
                    LastName = "Targaryen"
                },
                new Player
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Jon",
                    LastName = "Snow"
                },
                new Player
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Tyrion",
                    LastName = "Lannister"
                },
                new Player
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Brynden",
                    LastName = "Tully"
                },
                new Player
                {
                    Id = Guid.NewGuid(),
                    FirstName = "Arya",
                    LastName = "Stark"
                }
            };
            Table[] tables = {
                new Table
                {
                    Id = Guid.NewGuid(),
                    Name = "Winterfell",
                    Description = "The Pong in the North!"
                },
                new Table
                {
                    Id = Guid.NewGuid(),
                    Name = "Asshai",
                    Description = "By the Shadow"
                }
            };
            context.Players.AddRange(players);
            context.Tables.AddRange(tables);
            foreach (Table t in tables)
            {
                int numberOfGames = rand.Next(100),
                    i = 0;
                while (i <= numberOfGames)
                {
                    context.Games.Add(CreateRandomGame(t, players));
                    i++;
                }
            }
            context.SaveChanges();
        }

        private static Game CreateRandomGame(Table table, IEnumerable<Player> players)
        {
            Player[] randPlayers = players.OrderBy(_ => rand.Next()).Take(2).ToArray();
        
            // get two random ping pong scores (19 or less, to ensure that 21 is a valid win)
            int[] scores = { rand.Next(9,20), rand.Next(9,20) };

            // pick one of them to be the winner
            scores[rand.Next(0, 2)] = 21;

            // set a date some time in the last 30 days
            DateTime gameTime = DateTime.Now.AddSeconds(-1 * rand.Next(30 * 24 * 60 * 60));
            return new Game
            {
                Id = Guid.NewGuid(),
                Player1Id = randPlayers[0].Id,
                Player2Id = randPlayers[1].Id,
                Player1Points = scores[0],
                Player2Points = scores[1],
                TableId = table.Id,
                GameTime = gameTime
            };
        }
    }
}
