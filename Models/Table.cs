using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace pong.Models
{
    // A Ping Pong Table 
    public class Table
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }
        public List<Game> Games { get; set; }
    }
}
