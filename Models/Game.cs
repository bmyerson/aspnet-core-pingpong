using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace pong.Models
{
    // A Ping Pong game at a given table between two players
    public class Game
    {
        [Key]
        public Guid Id { get; set; }
        [Required]
        public Guid TableId { get; set; }
        [ForeignKey("TableId")]
        public Table Table { get; set; }
        [Required]
        public Guid Player1Id { get; set; }
        [ForeignKey("Player1Id")]
        public Player Player1 { get;set;}
        [Required]
        public Guid Player2Id { get; set; }
        [ForeignKey("Player2Id")]
        public Player Player2 { get;set;}
        public DateTime GameTime { get; set; }
        public int Player1Points { get; set; }
        public int Player2Points { get; set; }

        public Game() {
            this.GameTime = DateTime.Now;
        }

        public int TotalPoints () { 
            return this.Player1Points + this.Player2Points;
        }
    }
}
