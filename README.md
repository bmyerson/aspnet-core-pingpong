# What is this?

A lightweight Ping Pong scoreboard web application utilizing an ASP.net Core MVC web server (Kestrel) 
which provides an In-Memory DB, a JSON API as well as serving a static web site with an Angular 1.6 single-page
application. Upon starting, the server generates a bunch of data.

# Features

View/Add Tables

View/Add Games on a particular Table

View/Add Players

# Future Enhancements

[Elo](https://en.wikipedia.org/wiki/Elo_rating_system) ranking system

Angular Template Cache

Authentication/Authorization

Slack integration

Images for Users/Tables

Edit / Delete Entities

Persistence to DB

Live-reload during dev

Better Validation

# Pre-requisites

- [NPM](http://blog.npmjs.org/post/85484771375/how-to-install-npm)
- [ASP.net Core](https://www.microsoft.com/net/download/core)

# How to Build

`git clone git@bitbucket.org:bmyerson/aspnet-core-pingpong.git`

`cd aspnet-core-pingpong`

`npm install`

`dotnet restore`

`dotnet build`

# How to Run

`dotnet run`

`open http://localhost:5000`
